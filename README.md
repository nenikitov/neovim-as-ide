# Neovim as IDE

## Build

- Install [MARP](https://github.com/marp-team/marp)
- Run `build.sh`
- The presentation is available at `publc/index.html`

## References for assets

| Asset              | Source                                                              |
| ------------------ | ------------------------------------------------------------------- |
| **--- FONTS ---**  | **---**                                                             |
| Mononoki font      | [Web page](https://madmalik.github.io/mononoki/)                    |
| Jost font          | [Web page](https://fonts.google.com/specimen/Jost)                  |
| **--- IMAGES ---** | **---**                                                             |
| vi                 | [Web page](https://commons.wikimedia.org/wiki/File:Vi_logo.svg)     |
| Vim                | [Web page](https://commons.wikimedia.org/wiki/File:Vimlogo.svg)     |
| Neovim             | [Web page](https://commons.wikimedia.org/wiki/File:Neovim-mark.svg) |
| NvChad             | [Web page](https://nvchad.com/)                                     |
| Lunar VIM          | [Web page](https://www.lunarvim.org/)                               |

