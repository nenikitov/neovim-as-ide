import Footer from '../footer/Footer';
import Header from '../header/Header';
import Main from '../main/Main';
import ScrollUp from '../scroll-up/ScrollUp';

export default function App() {
  return (
    <>
      <Header />
      <Main />
      <Footer />

      <ScrollUp />
    </>
  );
}

