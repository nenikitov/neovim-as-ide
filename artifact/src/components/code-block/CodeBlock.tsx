import { PrismAsyncLight as SyntaxHighlighter } from 'react-syntax-highlighter';
import lua from 'react-syntax-highlighter/dist/esm/languages/prism/lua';
import * as prism from 'react-syntax-highlighter/dist/esm/styles/prism';

import './CodeBlock.css';

SyntaxHighlighter.registerLanguage('lua', lua);

interface CodeBlockProperties {
  code: string,
  language: 'lua'
}

export default function CodeBlock(props: CodeBlockProperties) {
  return (
    <SyntaxHighlighter
      language={props.language}
      style={prism.coldarkDark}
      showLineNumbers={true}
      showInlineLineNumbers={true}
      className='code-styled'
    >
      {props.code}
    </SyntaxHighlighter>
  );
}

