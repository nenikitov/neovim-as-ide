import './Footer.css';

import AssetGithub from '../../assets/logos/github.svg';
import AssetGitlab from '../../assets/logos/gitlab.svg';

export default function Footer() {
  return (
    <footer className='sections-lr'>
      <section>
        <h3>Mykyta Onipchenko</h3>
        <p>Student at Dawson College</p>
      </section>

      <nav>
        <a href='https://github.com/nenikitov'>
          <img src={AssetGitlab} alt='My GitHub' />
        </a>
        <a href='https://gitlab.com/nenikitov'>
          <img src={AssetGithub} alt='My GitLab' />
        </a>
      </nav>
    </footer>
  );
}

