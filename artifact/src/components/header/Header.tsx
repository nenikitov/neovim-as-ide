import './Header.css';

import { useState } from 'react';

import AssetNeovim from '../../assets/logos/neovim.svg';
import AssetSlides from '../../assets/icons/slides.svg';
import AssetPoints from '../../assets/icons/points.svg';
import TableOfContents from '../table-of-contents/TableOfContents';

export default function Header() {
  const [ tableOfContentsVisible, setTableOfContentsVisible ] = useState(false);

  return (
    <>
      <header className='sections-lr'>
        <section className='logo'>
          <img src={AssetNeovim} alt='Neovim logo' />
          <h1>Neovim as <span className='ide'>IDE</span></h1>
        </section>

        <nav>
          <a
            onClick={() => setTableOfContentsVisible(!tableOfContentsVisible)}
          >
            <img src={AssetPoints} alt='Table of contents' />
          </a>
          <a href='../slides/index.html'>
            <img src={AssetSlides} alt='Slides' />
          </a>
        </nav>
      </header>

      <section
        className={
          'pop-up-table-of-contents'
          + (tableOfContentsVisible ? ' visible' : '')
        }
        onMouseLeave={() => setTableOfContentsVisible(false)}
        onClick={() => setTableOfContentsVisible(false)}
      >
        <TableOfContents />
      </section>
    </>
  );
}
