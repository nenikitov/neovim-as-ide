import Introduction from './content/Introduction';
import WhyNeovim from './content/WhyNeovim';
import Configuration from './content/Configuration';
import Alternatives from './content/Alternatives';
import References from './content/References';

export default function Main() {
  return (
    <main>
      <Introduction />

      <WhyNeovim />
      <Configuration />
      <Alternatives />

      <References />
    </main>
  );
}

