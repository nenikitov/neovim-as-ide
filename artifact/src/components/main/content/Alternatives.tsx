import NavigableHeading from '../../table-of-contents/NavigableHeading';

export default function Alternatives() {
  return (
    <>
      <NavigableHeading level={2} title='Alternatives' />

      <NavigableHeading level={3} title='kickstart.nvim' />
      <p>
        <code><a href='https://github.com/nvim-lua/kickstart.nvim'>kickstart.nvim</a></code> is an
        example configuration. It is designed to be a great starting point for your own
        configuration if you are not looking forward to writing boiler-plate code to set up a
        plugin manager, while still being flexible.
      </p>

      <p>
        It also sets up some commonly plugins, many of which are in my list, such as:
      </p>
      <ul>
        <li>Treesitter</li>
        <li>Telescope</li>
        <li>Gitsigns</li>
      </ul>

      <p>
        Plus, it provides a good template how to structure your configuration
        across multiple files.
      </p>


      <NavigableHeading level={3} title='Neovim distributions' />
      <p>
        Neovim also comes in distributions, which include ready-to-use configuration. They are
        great if you want to start using Neovim but don&apos;t want to spend much time configuring
        it, or if you want to have a more specialized starting point for your configuration. Here
        are some popular ones:
      </p>
      <ul>
        <li><a href='https://nvchad.com'>NvChad</a></li>
        <li><a href='https://www.lunarvim.org/'>LunarVim</a></li>
        <li><a href='https://spacevim.org'>SpaceVim</a></li>
      </ul>
    </>
  );
}
