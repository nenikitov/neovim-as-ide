import CodeBlock from '../../code-block/CodeBlock';
import NavigableHeading from '../../table-of-contents/NavigableHeading';

const codeFileStructure = `\
- lua/        -- Path for custom modules
    - ...
- init.lua    -- Main configuration files\
`;

const codeOptions = `\
-- lua/options.lua

-- Shortcut to access vim options
local o = vim.opt

o.clipboard = 'unnamedplus'   -- Use system clipboard
o.number = true               -- Show line numbers
o.numberwidth = 4                 -- Line number column width

o.expandtab = true            -- Convert <TAB> to <SPACE>s automatically
o.shiftwidth = 4              -- Size of indentation
o.tabstop = 4                 -- Size of <TAB> character

-- More options here ...\
`;

const codeOptionsRequire = `\
-- init.lua

require('options')\
`;

const codeKeybindingsFunction = `\
-- For more, type :h vim.keymap.set
vim.keymap.set(
  modes,            -- Can be one string mode ('n') or an array ({ 'n', 'i' })
  key_combination,  -- String sequence of characters to press ('<C-v>a')
  action,           -- Sequence of characters to type ('<CMD>split<CR>') or a lua function (function () ... end)
  options           -- Misc keymap options
)\
`;

const codeKeybindings = `\
-- lua/keymap.lua

-- Wrapper to create keymaps more easily
local function map(modes, keys, func, description)
  vim.keymap.set(modes, keys, func, {
    noremap = true,
    silent = true,
    desc = description
  })
end

-- Set leader key
map('a', '<SPACE>', '<NOP>')
vim.g.mapleader = ' '

-- Quick splits
map('n', '<A-UP>', '<CMD>split<CR>', 'Split vertically')
-- ...

-- Quick navigation
map({ 'n', 't' }, '<C-k>', '<CMD>wincmd k<CR>', 'Go to split on top')
-- ...

-- ...\
`;

const codePacker = `\
-- lua/plugins/packer.lua

local fn = vim.fn
local clone_path = 'https://github.com/wbthomason/packer.nvim'
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim/'

-- Install packer if not already installed
local packer_bootstrapped = false
if fn.empty(fn.glob(install_path)) ~= 0 then
  fn.system {
    'git', 'clone',
    '--depth', '1',
    clone_path, install_path
  }
  vim.notify('Installed packer, reopen Neovim', vim.log.levels.WARN)
  vim.cmd('packadd packer.nvim')
  packer_bootstrapped = true
end

-- Sync packer if the plugins file was modified
vim.cmd([[
  augroup packer_use_config
    autocmd!
    autocmd BufWritePost packer.lua source <afile> | PackerSync
  augroup end
]])

-- Load and initialize
local packer_status, packer = pcall(require, 'packer')
if not packer_status then
  vim.notify('Error while loading packer', vim.log.levels.ERROR)
  return
end
packer.init {
  display = {
    open_fn = function()
      return require('packer.util').float { border = 'rounded' }
    end
  }
}

-- Install plugins
return packer.startup(function(use))
  -- Let packer manage itself
  use 'wbthomason/packer.nvim'

  -- Other plugins
  use('YOUR/PLUGIN/HERE')
  -- ...

  -- Set up the configuration if packer was just installed
  if packer_bootstrapped then
    packer.sync()
  end
end)\
`;

const codePackerUse = `\
use('USERNAME/REPO')    -- From git
use('PATH/TO/PLUGIN')   -- From local file system
use({                   -- With other options
  'YOUR/PLUGIN/HERE',
  requires = {
    'YOUR/DEPENDENCY/HERE'
  }
})\
`;

const codePluginInit = `\
-- lua/plugins/options/PLUGIN.lua

-- Load
local PLUGIN_status, PLUGIN = pcall(requrie, 'PLUGIN')
if not PLUGIN_status then
  vim.notify('PLUGIN not available', vim.log.levels.ERROR)
  return
end

-- Setup
PLUGIN.setup {
  -- Your options here
}\
`;

const codeColorscheme = `\
-- lua/colorscheme.lua

-- Shortcut to access vim command
local cmd = vim.cmd

-- Name of the colorscheme
local colorscheme = 'YOUR_COLOR_SCHEME'
-- Name of the colorscheme to fall back if the color scheme was not found
local colorscheme_fallback = 'default'

cmd('colorscheme ' .. colorscheme_fallback)
local colorscheme_status, _ = pcall(cmd, 'colorscheme ' .. colorscheme)
if not colorscheme_status then
    vim.notify('Color scheme ' .. colorscheme .. ' not available', vim.log.levels.WARN)
    return
end\
`;


export default function Configuration() {
  return (
    <>
      <NavigableHeading level={2} title='Custom configuration' />


      <NavigableHeading level={3} title='Prerequisites' />

      <p>
        First, you need to set up proper file structure for your Neovim configuration. Navigate to
        the directory where configuration is stored (<code>%LOCALAPPDATA%\nvim\</code> or <code>$HOME/.config/nvim/</code>
        ) and create this file structure:
      </p>
      <CodeBlock code={codeFileStructure} language='lua' />


      <NavigableHeading level={3} title='Default options' />

      <p>
        These are simple Vim settings like tab size, default file encoding, whether to show line
        numbers or no, and more.
      </p>
      <p>
        I don&apos;t like having everything in one file, so I split my configuration into modules.
        To do so, I put all my options in <code>lua/options.lua</code>:
      </p>
      <CodeBlock code={codeOptions} language='lua' />
      <p>
        Then, I can use this module from the main <code>init.lua</code> file (directories are
        separated with a <code>.</code>, <code>lua</code> directory is omitted, and so is the
        file extension):
      </p>
      <CodeBlock code={codeOptionsRequire} language='lua' />


      <NavigableHeading level={3} title='Keybinds' />
      <p>
        All keybinds are set with this built-in function:
      </p>
      <CodeBlock code={codeKeybindingsFunction} language='lua' />
      <p>
        Here is the example of useful shortcuts I have:
      </p>
      <CodeBlock code={codeKeybindings} language='lua' />


      <NavigableHeading level={3} title='Plugin manager' />
      <p>
        This is where fun begins - after this step you can easily add plugins to add extra
        functionality. To start, you need to select a plugin manager. Here are the most common
        ones: <a href='https://github.com/wbthomason/packer.nvim'><code>packer.nvim</code></a>, <a href='https://github.com/folke/lazy.nvim'><code>lazy.nvim</code></a>.
      </p>
      <p>
        Consult official documentation how to install it. I use <code>packer.nvim</code> for no
        particular reason, here is how I set it up:
      </p>
      <CodeBlock code={codePacker} language='lua' />
      <p>
        When you want to install a new plugin, specify it with the <code>use</code> function:
      </p>
      <CodeBlock code={codePackerUse} language='lua' />


      <NavigableHeading level={3} title='Plugin configuration' />
      <p>
        Usually, plugins require you to call their <code>setup</code> function to initialize it.
        I usually do it in a separate file per plugin or batch of related plugins. Consult
        plugin&apos;documentation on which options you can pass:
      </p>
      <CodeBlock code={codePluginInit} language='lua' />


      <NavigableHeading level={3} title='Color schemes' />
      <p>
        Color schemes are installed the same way as all other plugins. To activate a color scheme
        by default, I wrote this module:
      </p>
      <CodeBlock code={codeColorscheme} language='lua' />
      <p>
        For now, if you didn&apos;t yet install <a href='https://github.com/nvim-treesitter/nvim-treesitter'>Treesitter</a>,
        some color schemes will not appear correctly because default Neovim&apos;s syntax
        highlighting doesn&apos;t work well with some languages. But, when you get to install it,
        these color schemes will work much better. Check for compatibility with <code>Treesitter</code>
        in the official documentation.
      </p>


      <NavigableHeading level={3} title='Useful plugins' />
      <p>
        Here is a list of useful plugins in order in which I installed them in my config:
      </p>
      <ul>
        <li><a href='https://github.com/Mofiqul/vscode.nvim'><code>Mofiqul/vscode.nvim</code></a> - Color scheme inspired by default VSCode color scheme, works with Treesitter</li>
        <li><a href='https://github.com/hrsh7th/nvim-cmp'><code>hrsh7th/nvim-cmp</code></a> - Completion sources</li>
        <li><a href='https://github.com/rafamadriz/friendly-snippets'><code>rafamadriz/friendly-snippets</code></a> - VSCode like snippets</li>
        <li><a href='https://github.com/williamboman/mason.nvim'><code>williamboman/mason.nvim</code></a> - Package manager for language servers</li>
        <li><a href='https://github.com/neovim/nvim-lspconfig'><code>neovim/nvim-lspconfig</code></a> - Language servers configurations</li>
        <li><a href='https://github.com/nvim-telescope/telescope.nvim'><code>nvim-telescope/telescope.nvim</code></a> - Powerful fuzzy finder</li>
        <li><a href='https://github.com/nvim-treesitter/nvim-treesitter'><code>nvim-treesitter/nvim-treesitter</code></a> - Better syntax highlighting</li>
        <li><a href='https://github.com/nvim-tree/nvim-tree.lua'><code>nvim-tree/nvim-tree.lua</code></a> - File explorer</li>
        <li><a href='https://github.com/lewis6991/gitsigns.nvim'><code>lewis6991/gitsigns.nvim</code></a> - Git diffs near line numbers</li>
      </ul>


      <NavigableHeading level={3} title='Further reading' />
      <p>
        I use <a href='https://github.com/rockerBOO/awesome-neovim/blob/main/README.md'>this list</a> to search
        for new plugins.
      </p>
      <p>
        If you need a video series describing the configuration, I recommend <a href='https://www.youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ'>this playlist</a>
      </p>
    </>
  );
}

