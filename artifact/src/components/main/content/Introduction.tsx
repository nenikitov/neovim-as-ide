import NavigableHeading from '../../table-of-contents/NavigableHeading';

export default function Introduction() {
  return (
    <>
      <NavigableHeading level={2} title='Introduction' />

      <section>
        <p>
          Neovim is an extremely flexible and versatile code editor that rapidly became popular
          among both enthusiasts and professionals. Despite its origins as a text editor, and its
          lacking default configuration, Neovim can become a full-fledged development environment
          that competes with more traditional IDEs.
        </p>

        <p>
          Personally, I have been using Neovim as my primary editor for about four months now, and
          I find it&apos;s an excellent tool for a variety of tasks, including note-taking, general
          text editing, and programming. While there is an initial learning curve involved in
          personalizing it to suit individual preferences, I believe that the effort is worthwhile.
        </p>

        <p>
          I will discuss why I switched to Neovim, how it fits into my workflow, and how I set it
          up.
        </p>
      </section>
    </>
  );
}

