import NavigableHeading from '../../table-of-contents/NavigableHeading';

export default function References() {
  return (
    <>
      <NavigableHeading level={2} title='References' />

      <NavigableHeading level={3} title='Information' />
      <ul>
        <li><a href='https://en.wikipedia.org/wiki/Vi'>vi - Wikipedia</a></li>
        <li><a href='https://en.wikipedia.org/wiki/Vim_(text_editor)'>Vim (text editor) - Wikipedia</a></li>
        <li><a href='https://github.com/LunarVim/Neovim-from-scratch'>LunarVim/Neovim-from-scratch - GitHub</a></li>
        <li><a href='https://www.youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ'>Neovim from Scratch - YouTube</a></li>
        <li><a href='https://github.com/rockerBOO/awesome-neovim/blob/main/README.md'>Collections of awesome neovim plugins. </a></li>
        <li><a href='https://github.com/nvim-lua/kickstart.nvim'>kickstart.nvim</a></li>
        <li><a href='https://nvchad.com/'>NvChad - Neovim with lua is cool</a></li>
        <li><a href='https://www.lunarvim.org/'>LunarVim | LunarVim</a></li>
        <li><a href='https://www.spacevim.org/'>SpaceVim</a></li>
      </ul>


      <NavigableHeading level={3} title='Assets' />
      <ul>
        <li><a href='https://fonts.google.com/specimen/Jost'>Jost font - sans-serif text</a></li>
        <li><a href='https://madmalik.github.io/mononoki'>Mononoki font - monospaced text</a></li>
        <li><a href='https://commons.wikimedia.org/wiki/File:Neovim-mark.svg'>Neovim logo</a></li>
        <li><a href='https://github.com/logos'>GitHub logo</a></li>
        <li><a href='https://gitlab.com/press/press-kit'>GitLab logo</a></li>
        <li>All other assets are drawn by me</li>
      </ul>
    </>
  );
}

