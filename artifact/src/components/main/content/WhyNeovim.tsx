import NavigableHeading from '../../table-of-contents/NavigableHeading';

export default function WhyNeovim() {
  return (
    <>
      <NavigableHeading level={2} title='Why Neovim' />

      <NavigableHeading level={3} title='My workflow' />
      <p>
        I like using hotkeys when possible; I find them faster and less disruptive than moving the
        hand across the desk to pick up the mouse. Gradually, I&apos;ve been using hotkey-driven
        alternatives of the apps I use the most.
      </p>
      <p>
        I started using DuckDuckGo instead of Google only because it lets me cycle search results
        with <kbd>J</kbd> and <kbd>K</kbd>. I also installed Linux instead of Windows to try a
        tiling window manager, which automatically organizes your windows in a grid and lets you
        manipulate them (move, resize, focus) using only keyboard.
      </p>
      <p>
        This is why I decided to search for alternatives for my previous IDE of choice, VSCode:
        I wanted an editor that relies less on the mouse, for example, when managing splits, tabs,
        and cursor position.
      </p>


      <NavigableHeading level={3} title='Text editors with "modes"' />
      <p>
        Most text editors are non-modal, meaning they only have one mode for typing the text.
        In contrast, modal editors have multiple modes, each dedicated to a separate function.
        For example, Vi has <strong>normal</strong> mode for moving the cursor, <strong>insert</strong> mode
        for typing, <strong>visual</strong> mode for selecting,
        and many more.
      </p>
      <p>
        In modal editors, hotkeys are organized in layers, with different hotkey actions available
        depending on the context. As a result, Vi-style editors can design logical,
        easy-to-type shortcuts, that rarely relyg on keys like <kbd>SHIFT</kbd>, <kbd>CTRL</kbd>,
        and <kbd>ALT</kbd> to create more keyboard space.
      </p>


      <NavigableHeading level={3} title='Vim extensions for IDEs' />
      <p>
        VSCode has a fantastic extension ecosystem, and, of course, it has extensions to emulate
        Vi. This extension works well for most use cases, especially for moving the cursor and
        selecting chunks of text. However, some interface elements, like the side panel,
        are sill controlled with VSCode shortcuts, which can feel clunky when using Vi-style
        shortcuts for typing.
      </p>


      <NavigableHeading level={3} title='Pros/cons of Neovim' />
      <p>
        Here are some pros and cons of Neovim for me:
      </p>
      <ul>
        <li>Pros
          <ul>
            <li>Fully keyboard driven</li>
            <li>Available in terminal (easy to use over SSH)</li>
            <li>Both lightweight and robust</li>
            <li>You are free to choose and omit any feature depending on your workflow</li>
            <li>Expansive documentation about every feature of the editor</li>
            <li>Can work both as a simple text editor and a powerful IDE</li>
            <li>Configured in a programming language, which increases Customizability</li>
          </ul>
        </li>
        <li>Cons
          <ul>
            <li>Less officially supported than other IDEs like VSCode</li>
            <li>Configuring from scratch takes time</li>
            <li>
              Sometimes you need to work around other developers&apos; extensions to
              achieve configuration you want
            </li>
          </ul>
        </li>
      </ul>
    </>
  );
}

