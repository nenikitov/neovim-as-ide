import './ScrollUp.css';

import AssetArrow from '../../assets/icons/arrow.svg';
import { useState } from 'react';


export default function ScrollUp() {
  const [ visible, setVisible ] = useState(false);

  window.addEventListener('scroll', () => {
    setVisible(window.pageYOffset > 300);
  });

  return (
    <a
      onClick={() => {
        window.scrollTo({
          top: 0,
          behavior: 'smooth'
        });
      }}
    >
      <img
        id="scroll-up"
        className={visible ? 'visible' : ''}
        src={AssetArrow}
      />
    </a>
  );
}
