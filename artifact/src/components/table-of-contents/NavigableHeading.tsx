import { ReactElement } from 'react';
import { HeadingLevel, HeadingLevelTag } from './Types';

function titleToId(title: string): string {
  return title.toLocaleLowerCase().replace(/[^a-z0-9]+/g, '-');
}

type Title = string | ReactElement;

function getTitleText(title: Title): string {
  if (typeof(title) == 'string') {
    return title;
  } else if (title instanceof Array) {
    return title.map(getTitleText).join('');
  } else if (typeof(title) === 'object' && title) {
    return getTitleText(title.props.children);
  } else {
    return '';
  }
}


interface NavigableHeadingProps {
  level: HeadingLevel
  title: Title,
  className?: string
}

export default function NavigableHeading(props: NavigableHeadingProps) {
  const Tag: HeadingLevelTag = ('h' + props.level) as HeadingLevelTag;
  const id = titleToId(getTitleText(props.title));

  return (
    <section id={`${id}-anchor`} className='scroll-anchor'>
      <Tag
        id={id}
        className={props.className}
      >
        {props.title}
      </Tag>
    </section>
  );
}

