import './TableOfContents.css';
import { NestedHeading } from './Types';
import useHeadings from './UseHeadings';

function generateHeading(heading: NestedHeading) {
  return (
    <>
      <a
        href={`#${heading.heading.id}-anchor`}
        onClick={(e) => {
          e.preventDefault();
          const location = {
            top: window.pageYOffset,
            left: window.pageXOffset
          };
          window.location.hash = `${heading.heading.id}-anchor`;
          window.scrollTo({
            top: location.top,
            left: location.left
          });
          document.querySelector(`#${heading.heading.id}-anchor`)?.scrollIntoView({
            behavior: 'smooth'
          });
        }}
      >
        {heading.heading.textContent}
      </a>

      <ul>
        {heading.items.map((e, i) => {
          return <li key={i}>{generateHeading(e)}</li>;
        })}
      </ul>
    </>
  );
}

export default function TableOfContents() {
  const { headings } = useHeadings(2, 3);
  return (
    <nav aria-label='Table of contents' className='table-of-contents'>
      <ul>
        {headings.map((e, i) => {
          return <li key={i}>{generateHeading(e)}</li>;
        })}
      </ul>
    </nav>
  );
}

