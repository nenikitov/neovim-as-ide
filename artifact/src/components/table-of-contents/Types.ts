export type HeadingLevel = 1 | 2 | 3 | 4 | 5 | 6;
export type HeadingLevelTag = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
export interface NestedHeading {
  heading: HTMLHeadingElement
  items: NestedHeading[]
}

