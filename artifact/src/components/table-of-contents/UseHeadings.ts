import { useEffect, useState } from 'react';
import { NestedHeading, HeadingLevel } from './Types';

function getHeadingLevel(element: HTMLHeadingElement): HeadingLevel {
  return Number(element.tagName[1]) as HeadingLevel;
}

function getSubHeadings(heading: HTMLHeadingElement): HTMLHeadingElement[] {
  const level = getHeadingLevel(heading);
  if (level == 6) {
    return [];
  } else {
    const headings: HTMLHeadingElement[] = Array.from(document.querySelectorAll(`h${level}, h${level + 1}`));
    // Remove headings before
    headings.splice(0, headings.indexOf(heading) + 1);
    // Remove everything from next headings with same level
    const nextHeading = headings.findIndex((e) => getHeadingLevel(e) == level);
    if (nextHeading != -1) {
      headings.splice(
        nextHeading,
        headings.length
      );
    }
    // Remove all headings without ID
    return headings.filter((h) => Boolean(h.id));
  }
}

function getNestedHeading(heading: HTMLHeadingElement, end: HeadingLevel): NestedHeading {
  const nested: NestedHeading = {
    heading,
    items: []
  };
  if (getHeadingLevel(heading) != end) {
    for (const e of getSubHeadings(heading)) {
      nested.items.push(getNestedHeading(e, end));
    }
  }
  return nested;
}

function getHeadings(start: HeadingLevel, end: HeadingLevel): NestedHeading[] {
  return (
    Array.from(document.querySelectorAll(`h${start}`))
      .map((h) => getNestedHeading(h, end))
  );
}


export default function useHeadings(start: HeadingLevel, end: HeadingLevel) {
  const [ headings, setHeadings ] = useState<NestedHeading[]>([]);

  useEffect(() => {
    setHeadings(getHeadings(start, end));
  }, []);

  return { headings };
}

