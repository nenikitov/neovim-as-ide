#!/bin/bash

# CD into the root of the repo
PATH_REPO=$(dirname $(realpath $0))

# Create build directory
rm -r "./build/" 2> /dev/null
mkdir "./build/" 2> /dev/null
mkdir "./build/slides/" 2> /dev/null
mkdir "./build/artifact/" 2> /dev/null

# Build slides
npx -y @marp-team/marp-cli@latest \
  --html "./slides/slides.md" \
  --output "./build/slides/index.html"
cp -r "./slides/assets/" "./build/slides/"

# Build artifact
npm run build --prefix artifact
cp -r "./artifact/build/"* "./build/artifact/"

