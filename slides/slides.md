---
marp: true
paginate: true
style: |
    @font-face {
        font-family: Mononoki;
        src: url(./assets/mononoki.ttf);
        font-weight: normal;
    }
    @font-face {
        font-family: Mononoki;
        src: url(./assets/mononoki-bold.ttf);
        font-weight: bold;
    }
    @font-face {
        font-family: Jost;
        src: url(./assets/jost.otf);
        font-weight: normal;
    }
    @font-face {
        font-family: Jost;
        src: url(./assets/jost-bold.otf);
        font-weight: bold;
    }


    :root {
        background-color: #1E1E1E;
        color: #D4D4D4;
        font-family: Jost, sans;
    }

    section {
        justify-content: start;
    }
    section.center {
        justify-content: center;
    }

    h1 {
        color: #569CD6;
        font-family: Mononoki, mono;
        border-bottom: 0.2rem solid #569CD6;
    }
    h2 {
        color: #9CDCFE;
        font-family: Mononoki;
        border-bottom: 0.1rem solid #9CDCFE;
    }

    ul {
        padding-left: 1em;
    }
    li::marker {
        color: #569CD6;
    }

    a {
        color: #4EC9B0;
        text-decoration-line: underline !important;
    }

    strong {
        color: #FFFFFF;
    }

    img {
        background-color: transparent !important;
        filter:
            drop-shadow(0 0 0.3rem black)
            drop-shadow(0 0 0.3rem black);
    }
    figure {
        background-color: transparent !important;
        filter:
            drop-shadow(0 0 0.5rem black)
            drop-shadow(0 0 0.5rem black)
            !important;
    }

    code {
        font-family: Mononoki, mono;
        background-color: #303030;
        color: #D7BA7D;
        border: 0.1rem solid #3C3C3C;
        font-weight: bold;
    }

    tr {
        background-color: transparent !important;
        border: none !important;
    }
    tr:nth-child(odd) {
        background-color: #262626 !important;
    }
    td {
        border: none !important;
    }
    th {
        background-color: #303030 !important;
        color: #569CD6;
        border: none !important;
        border-bottom: 0.1rem solid #569CD6 !important;
    }



    .columns {
        display: flex;
        justify-content: space-evenly;
        gap: 1rem;
    }
    .columns * {
        flex: 1
    }

    .right-align-text {
        text-align: right;
    }
    .right-align {
        margin-left: auto;
        margin-right: 0;
    }

    .center-align {
        margin: auto;
    }

    .darker {
        color: #808080;
    }
---


<!-- Hello, and today I will present to you how and why you should use Neovim as your IDE. -->


<!-- _class: center -->

![bg left:40% 70%](./assets/neovim.svg)

# Neovim as IDE

<div class="right-align-text">

Create your own IDE from scratch.

<span class="darker">Mykyta Onipchenko</span>

</div>


---


<!-- But first, a bit of history of this small branch vi family of editors. -->
<!-- Vi was developed in 1976. The mouse wasn't fully adopted by computer users, so vi is fully keyboard driven. To do so, it used "modes", which we will discuss later in the presentation. -->
<!-- Vi quickly became a de facto editor of Unix-based operating systems, but it lacked many features. To remedy this issue, Vim was created in 1991, introducing such revolutionary features as syntax highlighting, and support for plugins written in VimScript. -->
<!-- Finally, in 2015, Neovim was created, improving plugin support with the introduction of a widely used programming language Lua. -->

# Brief History

<div class="columns">

<div>

## ![w:3em h:3em](./assets/vi.svg)

- **Vi**
    - Developed in **1976**
- Core features
    - Modal editor
    - Keyboard based

</div>

<div>

## ![w:3em h:3em](./assets/vim.svg)

- **Vim**
    - Developed in **1991**
- Core features
    - Syntax highlighting
    - VimScript
    - Plugin support

</div>

<div>

## ![w:3em h:3em](./assets/neovim.svg)

- **Neovim**
    - Developed in **2015**
- Core features
    - Lua
    - Better plugin support
    - More optimized

</div>

</div>


---


<!-- The most defining feature of vi-like editors is the use of modes. The 3 main modes are: -->
<!-- - Normal: for moving the cursor and text manipulation. -->
<!-- - Insert: For typing. -->
<!-- - Visual: For selecting. -->
<!-- As a result, you can use vi-like editors without touching the mouse. Plus, your hands will stay close to the home row. -->
<!-- Also, because each mode has its own set of hotkeys, you will rarely use the Ctrl key, which reduces the strain on the pinky. -->


# What is a "modal" editor?

<div class="columns">

<div>

## Main modes

- **Normal**
    - Move cursor
    - Manipulate text
- **Insert**
    - Type
- **Visual**
    - Select

</div>

<div>

## Result

- **Keyboard is everything**
    - No need for a mouse
    - Hands stay on the home row
- **More hotkeys**
    - Each mode has its own set
    - Hotkeys are "typed" as a sequence
    - Less strain on the pinky

</div>

</div>


---


<!-- Also, a lot of the hotkeys in vi have "motions", a way of specifying what you want to perform an action on without selecting. -->
<!-- For example, hotkeys to delete start with d. Moving the cursor to the left is done with h. So, do delete a character to the left, dh. -->
<!-- Similarly, to move until next =, you press t=. To delete until next =, it's dt=. -->
<!-- This can get really complex, like deleting everything inside 2nd outer parenthesis is d2i(. -->


# Motion keybindings

<div class="columns">

<div>

## Why

- Specifies a target for an action
- Combines movement with actions
- Builds more complex shortcuts

</div>

<div>

## Example

| Command                         | Shortcut |
|---------------------------------|----------|
| Delete                          | `d`      |
| Move left                       | `h`      |
| Delete a character to the left  | `dh`     |
| Move until next `=`             | `t=`     |
| Delete until next `=`           | `dt=`    |
| Delete inside second outer `()` | `d2i(`   |

</div>

</div>


---


<!-- With that out of the way, you can now see the pros and cons of Neovim. -->
<!-- It has fast and intuitive shortcuts. Plus, infinite customization options as it is configured in Lua, and you are free to implement/omit any features you'd like. It is also faster than traditional IDEs. -->
<!-- But the learning curve of the shortcuts is pretty steep for the first couple of weeks, and the setup can be long and confusing. -->


# Why Neovim

<div class="columns">

<div>

## Pros

- **Modal**
    - Fast and intuitive shortcuts
- **Configured in Lua**
    - Greater customization
    - Lua > VimScript
- **Configured from scratch**
    - You choose which features you want
    - Fast load times, instant feedback

</div>

<div>

## Cons

- **Difficulty**
    - Steep learning curve
- **Long setup**
    - Lots of plugins
    - No GUI to see all the options

</div>

</div>


---


<!-- Alternatively, you can choose one of the pre-configured Neovim distributions. -->
<!-- They are much easier to set up and they provide a great starting point for your config. -->
<!-- But they are slightly less customizable, and you don't feel as powerful as you would if you set it up yourself. -->


# Neovim Distributions

- **Pros**
    - Easy to set up
    - Good starting point for your own config
- **Cons**
    - Less customizable


---


<!-- Here are the 2 big distributions: NvChad: -->


# [NvChad](https://nvchad.com/)

<div class="center-align">

![w:30em](./assets/nvchad.webp)

</div>


---


<!-- And LunarVim. -->


# [LunarVim](https://nvchad.com/)

<div class="center-align">

![w:30em](./assets/lunarvim.png)

</div>


---


<!-- And if you want your config, here is a short list of what you need to do. -->
<!-- As a pre-requisite and step-by-step guide, find a good tutorial to follow. -->
<!-- Create an entry-point init.lua file in your configuration directory. -->
<!-- Set built-in options like indentation size and line numbers. -->
<!-- Install a plugin manager to make your life easier. -->
<!-- Configure Language Server Protocol to get snippets, language-aware suggestions, and diagnostics. -->
<!-- Install Treesitter to get better syntax highlighting. -->
<!-- Explore and install other plugins. -->


# Configuring from scratch

0. Follow a tutorial series like [this one](https://www.youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ)
1. Create `init.lua` in `%LOCALAPPDATA%\nvim` or `~/.config/nvim`
2. Configure built-in options like indentation size, line numbers, etc
3. Install a plugin manager ([packer.nvim](https://github.com/wbthomason/packer.nvim))
4. Configure your LSP (Snippets, warnings / errors)
5. Install [Treesitter](https://github.com/nvim-treesitter/nvim-treesitter) for better syntax highlighting
6. [Find](https://github.com/rockerBOO/awesome-neovim/blob/main/README.md) and install and configure other plugins


---


<!-- Thank you so much for listening. My presentation is available here if you want to explore the links. -->
<!-- And don't forget, exit with :wq -->


<!-- _class: center -->

# Thank you for listening

My presentation is available at [nenikitov.gitlab.io/neovim-as-ide/](nenikitov.gitlab.io/neovim-as-ide/)

And don't forget, exit is `:wq` in vi.


---


<!-- And here are my references. -->


# References

- [vi - Wikipedia](https://en.wikipedia.org/wiki/Vi)
- [Vim (text editor) - Wikipedia](https://en.wikipedia.org/wiki/Vim_(text_editor))
- [NvChad - Neovim with lua is cool](https://nvchad.com/)
- [LunarVim | LunarVim](https://www.lunarvim.org/)
- [LunarVim/Neovim-from-scratch - GitHub](https://github.com/LunarVim/Neovim-from-scratch)
- [Neovim from Scratch - YouTube](https://www.youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ)
- [List of cool plugins](https://github.com/rockerBOO/awesome-neovim/blob/main/README.md)

